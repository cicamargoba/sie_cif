/* sgen.c
 * makes a raw 8 or 16 bit digital sound signal of a waveform of a particular
 * frequency and sampling rate. Output is to a file, or /dev/dsp (linux)
 * Jim Jackson     Linux Version
 */

/*
 * Copyright (C) 1997-2008 Jim Jackson           jj@franjam.org.uk
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <ctype.h>
#include <sys/time.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <math.h>
#include "config.h"

unsigned int samplerate;         /* Samples/sec*/
unsigned int ratio;              /* used in pulse, sweep etc */
unsigned int antiphase;          /* 2nd channel in antiphase to 1st channel */
unsigned int Gain;               /* Amplification factor */
int stereo;                      /* stereo mono */
unsigned int afmt;               /* format for DAC  */
unsigned int frag_size;          /* size of DAC buffer fragments */
char dac[130];
int lbuf_flag,rbuf_flag;       
unsigned char *lbuf,*rbuf;       /* waveform bufs, left and right channels */
unsigned int lbuf_size;          /* size of 1 sec, 1 chan, waveform buffer */
unsigned char *plbuf;            /* play buffer - mixed left and right bufs */
unsigned char *silbuf;           /* silence buffer - used for timed output */
int plbuf_size;                  /* size of playback buffer */
long playtime,playsamples;       /* number of millisecs/samples to play
                                    0 means play for ever                  */
   int fd,st;

/*   playloop(fd,bf,bfn,N)   bf is a sample buffer of size bfn bytes.
 *   play the buffer cyclically (in a loop) in chunks of N */

playloop(fd,bf,bfn,N)
int fd;
unsigned char *bf;
int bfn;
int N;
{
  int i;
  unsigned char *p;
  
  for (;;) {
    for ( p=bf,i=0; i<= (bfn-N); i+=N) { 
      if (write(fd,p+i,N) < 0) return(0);
      /* check here if need to supend output or handle keypresses etc*/
    }
    if ((i!=bfn) && (write(fd,p+i,bfn-i)<0)) return(0);
  }
}

writecyclic(fd,bf,bfn,Ns)
int fd;
unsigned char *bf;
int bfn,Ns;
{
  int i;
  
  for (;;) {
    if (Ns<=bfn) { return(write(fd,bf,Ns)); }
    Ns-=bfn;
    if (write(fd,bf,bfn) < 0) return(-1);
  }
}

init_sound(){
   unsigned int i,n;
   char *p,*fnm;

   playsamples= 0;
   samplerate = 22050;
   stereo     = 1;
   antiphase  = 0;
   afmt       = AFMT_S16_LE;
   strncpy(dac,"/dev/dsp",sizeof(dac)-1);
   Gain       = 20;
   ratio      = -1;
   playtime   = 0;

   /* Check DAC parameters */
   i=afmt ; 
   n=stereo;
   if ((fd=DACopen(fnm=dac,"w",&samplerate,&i,&n))<0)
     if (fd<0) exit(err_rpt(errno,fnm));
   if ((frag_size=getfragsize(fd))<0)
     exit(err_rpt(errno,"Problem getting DAC Buffer size."));
   afmt=i;
   if (i!=afmt || n!=stereo) {
     exit(err_rpt(EINVAL,"Sound card doesn't support format requested."));
   }

   lbuf_size = samplerate << 1;
   lbuf_flag = rbuf_flag=0;
   lbuf      = (unsigned char *)malloc(lbuf_size);
   rbuf      = (unsigned char *)malloc(lbuf_size);
   plbuf     = (unsigned char *)malloc(plbuf_size=lbuf_size<<1);

   if ((rbuf==NULL) || (lbuf==NULL) || (plbuf==NULL)) {
      exit(err_rpt(ENOMEM,"Attempting to get buffers"));
   }

   mknull(lbuf,lbuf_size,0,0,samplerate,0,afmt);
   mknull(rbuf,lbuf_size,0,0,samplerate,0,afmt);
}
 
sound_generator(freq, freq2)
unsigned int freq, freq2;
{
   char *wf,*wf2;
   wf=wf2     = "sine";
   if (generate(wf,lbuf,lbuf_size,freq,Gain,samplerate,ratio,afmt)==0)
      exit(err_rpt(ENOSYS,wf));
   if (generate(wf2,rbuf,lbuf_size,freq2,Gain,samplerate,ratio,afmt)==0)
      exit(err_rpt(ENOSYS,wf2));

   mixplaybuf(plbuf,lbuf,rbuf,samplerate,afmt);
   playloop(fd,plbuf,plbuf_size,frag_size);
   free(lbuf); free(rbuf); free(plbuf); free(silbuf);
   exit(0);
}
