`timescale 1ns / 1ps
module edge_detector(clk, reset, din, pe);

input clk, reset, din;
output reg pe;

reg [1:0] state;	
reg [1:0] next_state; 

parameter zero=2'b00; 
parameter Edge=2'b01;
parameter one=2'b10;

  always@(posedge clk or negedge reset) begin
    if(!reset)
      state <= zero;
    else
      state <= next_state;
  end

  always@(state or din) begin
    next_state <= state;
    pe         <= 0;
    case(state)
      zero: begin
        pe <= 0;
        if(din)
          next_state <= Edge;
        else
          next_state <= zero;
      end

      Edge: begin
        pe <= 1;
        if(din)
          next_state <= one;
        else	
          next_state <= zero;
      end

      one: begin
        pe <= 0;
        if(~din)
          next_state <= zero;
        else	
          next_state <= one;
      end

      default:
        begin
          pe         <= 0;
          next_state <= zero;
        end
    endcase
  end

endmodule
