`timescale 1ns / 1ps
module event_cpu_interface (clk, reset, sram_data, addr, nwe, ncs, noe, irq, sig_in1, sig_in2);

    input   clk, reset, nwe, ncs, noe, sig_in1, sig_in2;
    input   [10:0] addr;
    inout   [7:0] sram_data;
    output  irq;


    // bram interfaz signals
    reg   we;
    reg   w_st=0;
    reg   [7:0]   wrBus;
    wire  [7:0]   rdBus;

    reg   sncs, snwe, snoe, ssig_in1, ssig_in2;
    reg   [10:0] buffer_addr; 
    reg   [7:0]	buffer_data;   


   // interefaz signals assignments
    wire   T = ~noe | ncs;
    assign sram_data = T?8'bZ:rdBus;

    // synchronize assignment
    always  @(negedge clk)
    begin
        sncs     <= ncs;
        snwe     <= nwe;
        snoe     <= noe;
        ssig_in1 <= sig_in1;
        ssig_in2 <= sig_in2;
        buffer_data <= sram_data;
        buffer_addr <= addr;
    end

    // write access cpu to bram
    always @(posedge clk)
    if(!reset) {w_st, we, wrBus} <= 0;
      else begin
        wrBus <= buffer_data;
        case (w_st)
          0: begin
              we <= 0;
              if(sncs | snwe) w_st <= 1;
          end
          1: begin
            if(~(sncs | snwe)) begin
              we    <= 1;
              w_st  <= 0;
            end	
            else we <= 0;
          end
        endcase
      end


 //event_peripheral event_peripheral1 ( clk, reset, ~sncs, we, snoe, irq, ssig_in, buffer_addr, rdBus, wrBus);

 event2_peripheral event2_peripheral1(  clk, reset, ~sncs, we, snoe, irq, ssig_in1, ssig_in2, buffer_addr, rdBus, wrBus);


endmodule
