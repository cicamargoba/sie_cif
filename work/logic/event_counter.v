`timescale 1ns / 1ps
module event_counter (reset, clk, load, sig_in, load_count, count_in, count_out, reg_out);


input  reset, clk, load, sig_in, load_count;
input  [31:0]count_in;
output [31:0]count_out;
output [31:0]reg_out;

reg [1:0]   w_st0=0;
reg reset_count, inc_count, load_reg;
wire in_pulse;

  always @(posedge clk or negedge reset)
    if(!reset) begin
      reset_count <= 0; inc_count <= 0; w_st0 <= 0;
    end
    else begin
      case(w_st0)
        0: begin
             reset_count <= 0; inc_count <= 0; load_reg <= 0;                 // Reset counter
             w_st0 <= 1;
           end 

        1: begin
             reset_count <= 1;
             if (load) begin                          
               load_reg    <= 1;                   // Load register if load from timer is set
               inc_count   <= 0;                                   
               w_st0       <= 3;
             end
             else begin
               load_reg    <= 0;
               if (in_pulse)                  
                 inc_count   <= 1;                 // If load from timer is not set & sign_in is set then increase counter
               else
                 inc_count   <= 0;
               w_st0       <= 2;
             end
           end

        2: begin
             reset_count   <= 1;
             load_reg      <= 0;
               inc_count   <= 0;                                 
               w_st0       <= 1;
           end

        3: begin
             load_reg      <= 0;                
             inc_count     <= 0;
             reset_count   <= 0;                   // Reset Counter after load register                                   
             w_st0         <= 1;
           end

        default: begin
             reset_count <= 0; inc_count <= 0; load_reg <= 0;
             w_st0 <= 0;
           end
      endcase
    end

    counter  counter    (.clk(clk), .reset(reset_count), .load(load_count), .inc(inc_count), .d(count_in), .q(count_out));
    register register   (.clk(clk), .reset(reset), .load(load_reg), .d(count_out), .q(reg_out));
    edge_detector edge1 (.clk(clk), .reset(reset), .din(sig_in), .pe(in_pulse));


endmodule






