`timescale 1ns / 1ps
module timing_generator (reset, clk, clk_out);

input   reset;	       // Reset (H)
input   clk;	       // Reloj del sistema
output	clk_out;       // Frecuencia de salida

// Variables
reg	[15:0]	div;	// Contador del divisor 
reg	[15:0]	k_div;	// Factor de division
reg		clk_out;// Frecuencia de salida

parameter SCALE_FACTOR=32'h0000C350;

always @(posedge clk or negedge reset)

begin
  if (!reset) begin
    div   <= 0;
    k_div <= SCALE_FACTOR;  // Reloj de 50MHZ
  end
  else begin
    if (div==k_div) begin
      div <= 0;
      clk_out <= 1;
    end
    else begin
      div <= div + 1;
      clk_out <= 0;
    end
  end
end

endmodule
