`timescale 1ns / 1ps
module event2_timer (reset, clk, max_count, load_c_count, count_c_in, count_c_out, irq_load, 
                    sig_in1, sig_in2, load_a1_count,  load_a2_count, count_a1_in, count_a2_in,
                    count_a1_out, count_a2_out, reg1_out, reg2_out);


input  reset, clk;
input  sig_in1, sig_in2;
input  load_a1_count, load_a2_count;
input  [31:0]count_a1_in;
input  [31:0]count_a2_in;

output [31:0]count_a1_out;
output [31:0]count_a2_out;
output [31:0]reg1_out;
output [31:0]reg2_out;

input  load_c_count;
input  [31:0]max_count;
input  [31:0]count_c_in;
output [31:0]count_c_out;
output irq_load;

wire load;

assign load = irq_load;

 event_counter event1 (reset, clk, load, sig_in1, load_a1_count, count_a1_in, count_a1_out, reg1_out);
 event_counter event2 (reset, clk, load, sig_in2, load_a2_count, count_a2_in, count_a2_out, reg2_out);
 timer         tmer1  (reset, clk, max_count, load_c_count, count_c_in, count_c_out, irq_load);

endmodule
  
