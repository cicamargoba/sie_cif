`timescale 1ns / 1ps
module event_peripheral(  clk, reset, cs, we, noe, irq, sig_in,
                        addr, rdBus, wrBus);
                                    
    input   clk, reset, cs, we, noe, sig_in;
    input   [3:0] addr;
    input   [7:0]  wrBus;
    output  [7:0]  rdBus;
    output  irq;

    reg     [7:0]  rdBus;
    wire    we;
    
    reg  [31:0] REGISTER_D; reg [31:0] COUNTER_A_IN; reg  [31:0] COUNTER_C_IN;
    wire [31:0] REGISTER_B; wire [31:0] COUNTER_A_OUT; wire [31:0] COUNTER_C_OUT;
 
    reg  ld_count_c = 0, ld_count_a = 0;


    /**************************************************************************/
    // REGISTER BANK:   Write control
    always @(negedge clk or negedge reset)
    begin
        if(!reset) 
            {ld_count_c, ld_count_a, COUNTER_C_IN, REGISTER_D, COUNTER_A_IN} <= 0;
        else if(we & cs) begin
          case (addr[3:0])   
                    0: begin COUNTER_C_IN[7:0]   <= wrBus; ld_count_c <= 1; ld_count_a <= 0;  end
                    1: begin COUNTER_C_IN[15:8]  <= wrBus; ld_count_c <= 1; ld_count_a <= 0;  end
                    2: begin COUNTER_C_IN[23:16] <= wrBus; ld_count_c <= 1; ld_count_a <= 0;  end            
                    3: begin COUNTER_C_IN[31:24] <= wrBus; ld_count_c <= 1; ld_count_a <= 0;  end
      
                    4: begin REGISTER_D[7:0]     <= wrBus; ld_count_c <= 0; ld_count_a <= 0;  end
                    5: begin REGISTER_D[15:8]    <= wrBus; ld_count_c <= 0; ld_count_a <= 0;  end
                    6: begin REGISTER_D[23:16]   <= wrBus; ld_count_c <= 0; ld_count_a <= 0;  end
                    7: begin REGISTER_D[31:24]   <= wrBus; ld_count_c <= 0; ld_count_a <= 0;  end

                    8: begin COUNTER_A_IN[7:0]   <= wrBus; ld_count_c <= 0; ld_count_a <= 1;  end
                    9: begin COUNTER_A_IN[15:8]  <= wrBus; ld_count_c <= 0; ld_count_a <= 1;  end
                   10: begin COUNTER_A_IN[23:16] <= wrBus; ld_count_c <= 0; ld_count_a <= 1;  end
                   11: begin COUNTER_A_IN[31:24] <= wrBus; ld_count_c <= 0; ld_count_a <= 1;  end

              default: begin 
                       COUNTER_C_IN <= COUNTER_C_IN; REGISTER_D <= REGISTER_D; 
                       COUNTER_A_IN <= COUNTER_A_IN;
                       ld_count_c <= 0; ld_count_a <= 0; 
                       end     
          endcase 
        end 
        else begin 
             COUNTER_C_IN <= COUNTER_C_IN; REGISTER_D <= REGISTER_D;
             COUNTER_A_IN <= COUNTER_A_IN;
             ld_count_c <= 0; ld_count_a <= 0; 
        end
    end            

    // REGISTER BANK:   Read control    
    always @(posedge clk or negedge reset)
        if(!reset) 
            {rdBus} <= 0;
        else begin //if(cs & noe) begin
          case (addr[3:0])   
                  0: begin rdBus  <= COUNTER_C_OUT[7:0];    end
                  1: begin rdBus  <= COUNTER_C_OUT[15:8];   end
                  2: begin rdBus  <= COUNTER_C_OUT[23:16];  end            
                  3: begin rdBus  <= COUNTER_C_OUT[31:24];  end

                  4: begin rdBus  <= REGISTER_D[7:0];       end
                  5: begin rdBus  <= REGISTER_D[15:8];      end
                  6: begin rdBus  <= REGISTER_D[23:16];     end            
                  7: begin rdBus  <= REGISTER_D[31:24];     end

                  8: begin rdBus  <= COUNTER_A_OUT[7:0];    end
                  9: begin rdBus  <= COUNTER_A_OUT[15:8];   end
                 10: begin rdBus  <= COUNTER_A_OUT[23:16];  end            
                 11: begin rdBus  <= COUNTER_A_OUT[31:24];  end

                 12: begin rdBus  <= REGISTER_B[7:0];       end
                 13: begin rdBus  <= REGISTER_B[15:8];      end
                 14: begin rdBus  <= REGISTER_B[23:16];     end            
                 15: begin rdBus  <= REGISTER_B[31:24];     end

            default: begin {rdBus}  <= 0;       end
          endcase 
        end


  event_timer event_timer1 (reset, clk, REGISTER_D, ld_count_c, COUNTER_C_IN, COUNTER_C_OUT, irq, 
                    sig_in, ld_count_a, COUNTER_A_IN, COUNTER_A_OUT, REGISTER_B);

endmodule
