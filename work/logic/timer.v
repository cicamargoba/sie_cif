`timescale 1ns / 1ps
module timer (reset, clk, max_count, load_count, count_c_in, count_c_out, irq_load);


input  reset, clk, load_count;
input  [31:0]max_count;
input  [31:0]count_c_in;
output [31:0]count_c_out;
output irq_load;

reg [1:0]   w_st0=0;
reg irq_load, reset_count, inc_count;
wire eq_count;
wire clk_ms;

  assign eq_count = (max_count == count_c_out) ? 1 : 0;

  always @(posedge clk or negedge reset)
    if(!reset) begin
      irq_load <= 0; reset_count <= 0; inc_count <= 0; w_st0 <= 0;
    end
    else begin
      case(w_st0)
        0: begin
             irq_load <= 0; reset_count <= 0; inc_count <= 0;  // Reset counter
             w_st0 <= 1;
           end

        1: begin
             inc_count <= 0;                                  
             if (eq_count) begin                               // Check if counter reach the max count.
               irq_load <= 1; reset_count <= 0;                // Set irq signal & reset counter.
             end
             else begin
               irq_load <= 0; reset_count <= 1;                
             end
             w_st0 <= 2;
           end

        2: begin
              irq_load <= 0; reset_count <= 1;                 
             if(clk_ms) begin                                  // wait 1ms
               inc_count <= 1;                                 // inc counter
               w_st0 <= 3;
             end 
             else begin
               inc_count <= 0;
               w_st0 <= 2;
             end  
           end

        3: begin
             irq_load <= 0; reset_count <= 1; inc_count <= 0;
             w_st0 <= 1;
           end

        default: begin
             irq_load <= 0; reset_count <= 0; inc_count <= 0;
             w_st0 <= 0;
           end
      endcase
    end

    counter counter_c (.clk(clk), .reset(reset_count), .load(load_count), .inc(inc_count), .d(count_c_in), .q(count_c_out));
    //timing_generator  #(32'h000000A) timing (.clk(clk), .reset(reset), .clk_out(clk_ms));
    timing_generator  timing (.clk(clk), .reset(reset), .clk_out(clk_ms));

endmodule






