`timescale 1ns / 1ps

module timing_generator_TB;
   reg clk;
   reg reset;
   wire clk_out;

   timing_generator #(32'h000000A) uut ( .clk(clk), .reset(reset),
       .clk_out(clk_out)
   );
   parameter PERIOD  = 20;
   parameter real DUTY_CYCLE = 0.5;
   parameter OFFSET  = 0;
   parameter TSET    = 10;
   parameter THLD    = 3;
   parameter NWS     = 3;
   parameter CAM_OFF = 4000;
	 
   reg [15:0] i;
   reg [15:0] j;
   reg [15:0] k;
   reg [15:0] data_tx;	 
   event reset_trigger;
   event reset_done_trigger;

    initial begin // Reset the system, Start the image capture process
      forever begin 
        @ (reset_trigger);
        @ (negedge clk);
        reset = 1;
        @ (negedge clk);
        reset = 0;
        @ (negedge clk);
        reset = 1;
        -> reset_done_trigger;
      end
    end
	 
   initial begin  // Initialize Inputs
     clk = 0;
   end

   initial  begin  // Process for clk
     #OFFSET;
     forever
     begin
       clk = 1'b0;
       #(PERIOD-(PERIOD*DUTY_CYCLE)) clk = 1'b1;
       #(PERIOD*DUTY_CYCLE);
     end
   end

 initial begin: TEST_CASE	
   #10 -> reset_trigger;
   @ (reset_done_trigger); 
   for(i=0; i<500000; i=i+1) begin
     @ (posedge clk);
   end
 end

 initial begin
   $dumpfile("timing_generator_TB.vcd");
   $dumpvars(-1, uut);
   #10 -> reset_trigger;
   #((PERIOD*DUTY_CYCLE)*1000) $finish;
 end



endmodule

