`timescale 1ns / 1ps
module register(clk,reset,load,d,q);
  parameter N = 32;
   
  input clk,reset,load;
  input  [N-1:0] d;
  output [N-1:0] q;
  reg    [N-1:0] q;

  always @(posedge clk or negedge reset)
    if(!reset) 
      q <= 0;
    else if(load) 
      q <= d;
endmodule
