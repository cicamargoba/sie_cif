`timescale 1ns / 1ps
module event_timer (reset, clk, max_count, load_c_count, count_c_in, count_c_out, irq_load, 
                    sig_in, load_a_count, count_a_in, count_a_out, reg_out);


input  reset, clk;
input  sig_in, load_a_count;
input  [31:0]count_a_in;
output [31:0]count_a_out;
output [31:0]reg_out;

input  load_c_count;
input  [31:0]max_count;
input  [31:0]count_c_in;
output [31:0]count_c_out;
output irq_load;

wire load;

assign load = irq_load;

 event_counter event1 (reset, clk, load, sig_in, load_a_count, count_a_in, count_a_out, reg_out);
 timer         tmer1  (reset, clk, max_count, load_c_count, count_c_in, count_c_out, irq_load);

endmodule
  
