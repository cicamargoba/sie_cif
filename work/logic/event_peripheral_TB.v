`timescale 1ns / 1ps

module event_peripheral_TB;

  reg   clk; 
  reg   reset;
  reg   cs;
  reg   we;
  reg   noe;
  reg   sig_in;
  reg   [10:0] addr;
  reg   [7:0]  wrBus;
  wire  [7:0]  rdBus;
  wire  irq;

  event_peripheral uut (clk, reset, cs, we, noe, irq, sig_in, addr, rdBus, wrBus);

   parameter PERIOD  = 20;
   parameter real DUTY_CYCLE = 0.5;
   parameter OFFSET  = 0;
   parameter TSET    = 10;
   parameter THLD    = 3;
   parameter NWS     = 3;
   parameter CAM_OFF = 4000;
	 
   reg [15:0] i;
   reg [15:0] j;
   reg [15:0] k;
   reg [31:0] data;	 
   event reset_trigger;
   event reset_done_trigger;

    initial begin // Reset the system, Start the image capture process
      forever begin 
        @ (reset_trigger);
        @ (negedge clk);
        reset = 1;
        @ (negedge clk);
        reset = 0;
        @ (negedge clk);
        reset = 1;
        -> reset_done_trigger;
      end
    end

   initial begin  // Initialize Inputs
     clk = 0; cs = 0; we = 0; noe = 0; sig_in = 0;
     addr = 0; wrBus = 0;
   end

   initial  begin  // Process for clk
     #OFFSET;
     forever
     begin
       clk = 1'b0;
       #(PERIOD-(PERIOD*DUTY_CYCLE)) clk = 1'b1;
       #(PERIOD*DUTY_CYCLE);
     end
   end


   initial  begin  // Process for sig_in
     #1000;
     forever
     begin
       for(i=0; i<20; i=i+1) begin
         @ (posedge clk);
         sig_in = 0;
       end
       for(i=0; i<40; i=i+1) begin
         @ (posedge clk);
         sig_in = 1;
       end
       for(i=0; i<10; i=i+1) begin
         @ (posedge clk);
         sig_in = 0;
       end
       for(i=0; i<60; i=i+1) begin
         @ (posedge clk);
         sig_in = 1;
       end
     end
   end


 initial begin: TEST_CASE	
   #10 -> reset_trigger;
   @ (reset_done_trigger); 
   addr  = 4;
   data  = 32'h0000000A;   
   for(i=0; i<4; i=i+1) begin
     wrBus = (data) & (32'h000000ff);
     @ (posedge clk);
     cs = 1;
     we = 1;
     @ (posedge clk);
     cs = 0;
     we = 0;
     addr = addr + 1;
     data = data >> 8;
   end

   for(i=0; i<500000; i=i+1) begin
     @ (posedge clk);
   end

 end


 initial begin
   $dumpfile("event_peripheral_TB.vcd");
   $dumpvars(-1, uut);
   #10 -> reset_trigger;
   #((PERIOD*DUTY_CYCLE)*1000) $finish;
 end



endmodule



/*

*/



