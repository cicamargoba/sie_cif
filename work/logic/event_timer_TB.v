`timescale 1ns / 1ps

module event_timer_TB;

   reg clk;
   reg reset;
   reg load_c_count;
   reg [31:0] max_count;
   reg [31:0] count_c_in;
   wire [31:0] count_c_out;
   wire irq_load;

   reg sig_in;
   reg load_a_count;
   reg  [31:0] count_a_in;
   wire [31:0] count_a_out;
   wire [31:0] reg_out;

   event_timer uut ( .clk(clk), .reset(reset), .max_count(max_count), .load_c_count(load_c_count), .count_c_in(count_c_in), 
               .count_c_out(count_c_out), .irq_load(irq_load), .sig_in(sig_in), .load_a_count(load_a_count), .count_a_in(count_a_in), 
               .count_a_out(count_a_out), .reg_out(reg_out)
   );
   parameter PERIOD  = 20;
   parameter real DUTY_CYCLE = 0.5;
   parameter OFFSET  = 0;
   parameter TSET    = 10;
   parameter THLD    = 3;
   parameter NWS     = 3;
   parameter CAM_OFF = 4000;
	 
   reg [15:0] i;
   reg [15:0] j;
   reg [15:0] k;
   reg [15:0] data_tx;	 
   event reset_trigger;
   event reset_done_trigger;

    initial begin // Reset the system, Start the image capture process
      forever begin 
        @ (reset_trigger);
        @ (negedge clk);
        reset = 1;
        @ (negedge clk);
        reset = 0;
        @ (negedge clk);
        reset = 1;
        -> reset_done_trigger;
      end
    end
	 
   initial begin  // Initialize Inputs
     clk          = 0;
     max_count    = 10;
     load_c_count = 0;
     count_c_in   = 0;
     sig_in       = 0;
     load_a_count = 0;
     count_a_in   = 0;
   end

   initial  begin  // Process for clk
     #OFFSET;
     forever
     begin
       clk = 1'b0;
       #(PERIOD-(PERIOD*DUTY_CYCLE)) clk = 1'b1;
       #(PERIOD*DUTY_CYCLE);
     end
   end
   initial  begin  // Process for sig_in
     #OFFSET;
     forever
     begin
       for(i=0; i<20; i=i+1) begin
         @ (posedge clk);
         sig_in = 0;
       end
       for(i=0; i<40; i=i+1) begin
         @ (posedge clk);
         sig_in = 1;
       end
       for(i=0; i<10; i=i+1) begin
         @ (posedge clk);
         sig_in = 0;
       end
       for(i=0; i<60; i=i+1) begin
         @ (posedge clk);
         sig_in = 1;
       end
     end
   end


 initial begin: TEST_CASE	
   #10 -> reset_trigger;
   @ (reset_done_trigger); 
   for(i=0; i<500000; i=i+1) begin
     @ (posedge clk);
   end
 end

 initial begin
   $dumpfile("event_timer_TB.vcd");
   $dumpvars(-1, uut);
   #10 -> reset_trigger;
   #((PERIOD*DUTY_CYCLE)*1000) $finish;
 end



endmodule

