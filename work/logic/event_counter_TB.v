`timescale 1ns / 1ps
//TODO: Check why load sognal is ignored by simulator
module event_counter_TB;

   reg clk;
   reg reset;
   reg load;
   reg sig_in;
   reg load_count;
   reg  [31:0] count_in;
   wire [31:0] count_out;
   wire [31:0] reg_out;


   event_counter uut ( .clk(clk), .reset(reset), .load(load), .sig_in(sig_in), .load_count(load_count), .count_in(count_in), 
               .count_out(count_out), .reg_out(reg_out)
   );
   parameter PERIOD  = 20;
   parameter real DUTY_CYCLE = 0.5;
   parameter OFFSET  = 0;
   parameter TSET    = 10;
   parameter THLD    = 3;
   parameter NWS     = 3;
   parameter CAM_OFF = 4000;
	 
   reg [15:0] i;
   reg [15:0] j;
   reg [15:0] k;
   reg [15:0] data_tx;	 
   event reset_trigger;
   event reset_done_trigger;

    initial begin // Reset the system, Start the image capture process
      forever begin 
        @ (reset_trigger);
        @ (negedge clk);
        reset = 1;
        @ (negedge clk);
        reset = 0;
        @ (negedge clk);
        reset = 1;
        -> reset_done_trigger;
      end
    end


   initial  begin  // Process for clk
     #OFFSET;
     forever
     begin
       clk = 1'b0;
       #(PERIOD-(PERIOD*DUTY_CYCLE)) clk = 1'b1;
       #(PERIOD*DUTY_CYCLE);
     end
   end

   initial  begin  // Process for sig_in
     #OFFSET;
     forever
     begin
       for(i=0; i<4; i=i+1) begin
         @ (posedge clk);
         sig_in = 0;
       end
       for(i=0; i<6; i=i+1) begin
         @ (posedge clk);
         sig_in = 1;
       end
     end
   end

   initial  begin  // Process for load (from timer)
     #OFFSET;
     forever
     begin
       for(i=0; i<20; i=i+1) begin
         @ (posedge clk);
         load = 0;
       end
       for(i=0; i<1; i=i+1) begin
         @ (posedge clk);
         load = 1;
       end
     end
   end


 initial begin: TEST_CASE
  #0
     clk        = 0;
     load       = 0;
     sig_in     = 0;
     load_count = 0;
     count_in   = 0;	
   #10 -> reset_trigger;
   @ (reset_done_trigger); 
   for(i=0; i<500000; i=i+1) begin
     @ (posedge clk);
   end
 end

 initial begin
   $dumpfile("event_counter_TB.vcd");
   $dumpvars(-1, uut);
   #10 -> reset_trigger;
   #((PERIOD*DUTY_CYCLE)*2000) $finish;
 end



endmodule

